#pragma once

#define CAT(x, y) CAT_I(x, y)
#define CAT_I(x, y) x ## y
#
#define APPLY(macro, args) APPLY_I(macro, args)
#define APPLY_I(macro, args) macro args
#
#define STRIP_PARENS(x) EVAL((STRIP_PARENS_I x), x)
#define STRIP_PARENS_I(...) 1,1
#
#define EVAL(test, x) EVAL_I(test, x)
#define EVAL_I(test, x) MAYBE_STRIP_PARENS(TEST_ARITY test, x)
#
#define TEST_ARITY(...) APPLY(TEST_ARITY_I, (__VA_ARGS__, 2, 1))
#define TEST_ARITY_I(a,b,c,...) c
#
#define MAYBE_STRIP_PARENS(cond, x) MAYBE_STRIP_PARENS_I(cond, x)
#define MAYBE_STRIP_PARENS_I(cond, x) CAT(MAYBE_STRIP_PARENS_, cond)(x)
#
#define MAYBE_STRIP_PARENS_1(x) x
#define MAYBE_STRIP_PARENS_2(x) APPLY(MAYBE_STRIP_PARENS_2_I, x)
#define MAYBE_STRIP_PARENS_2_I(...) __VA_ARGS__
#
#define PARTANSSTR(...) #__VA_ARGS__
#define PARTANSSTRX(...) PARTANSSTR(__VA_ARGS__)

# define VAR0(idx, ...) VA_DISPATCHER(VAR, __VA_ARGS__)(__VA_ARGS__, idx)
# define VAR01(T, idx) field##idx = idx
# define VAR02(T, N, idx) N = idx
#
# define TYPEEXT(T,...) T
# define TYPECALL(T) TYPEEXT(T)
# define EXTRACTTYPE(X) TYPECALL(STRIP_PARENS(X))
#
# define EVALVAR(T1, idx) VAR0(idx, STRIP_PARENS(T1))
#
# define LIST01(T1) \
  EVALVAR(T1,0)
# define LIST02(T1,T2) \
  EVALVAR(T2,1),LIST01(T1)
# define LIST03(T1,T2,T3) \
  EVALVAR(T3,2),LIST02(T1,T2)
# define LIST04(T1,T2,T3,T4) \
  EVALVAR(T4,3),LIST03(T1,T2,T3)
# define LIST05(T1,T2,T3,T4,T5) \
  EVALVAR(T5,4),LIST04(T1,T2,T3,T4)
# define LIST06(T1,T2,T3,T4,T5,T6) \
  EVALVAR(T6,5),LIST05(T1,T2,T3,T4,T5)
# define LIST07(T1,T2,T3,T4,T5,T6,T7) \
  EVALVAR(T7,6),LIST06(T1,T2,T3,T4,T5,T6)
# define LIST08(T1,T2,T3,T4,T5,T6,T7,T8) \
  EVALVAR(T8,7),LIST07(T1,T2,T3,T4,T5,T6,T7)
# define LIST09(T1,T2,T3,T4,T5,T6,T7,T8,T9) \
  EVALVAR(T9,7),LIST08(T1,T2,T3,T4,T5,T6,T7,T8)
#
# define MakeTypeList(...) struct {                                \
    /* Using an enum generates nice compile errors if not unique*/ \
    typedef enum {VA_DISPATCHER(LIST, __VA_ARGS__)(__VA_ARGS__)} FIELD_NAMES;          \
    /* The actual TypeList */                                      \
    typedef MakeTypeList0(__VA_ARGS__) TL;                         \
    typedef TL::Head Head;                                         \
    typedef TL::Tail Tail;                                         \
}

# ifndef MakeTypeList0
#  define MakeTypeList0(...) VA_DISPATCHER(MakeTypeList, __VA_ARGS__)(__VA_ARGS__)
# endif
#
# ifndef VA_ARG_SHIFTER
#  define VA_ARG_SHIFTER(...) VA_ARG_SHIFTER_COUNTER(__VA_ARGS__,19,18,17,16,15,14,13,12,11,10,09,08,07,06,05,04,03,02,01)
#  define VA_ARG_SHIFTER_COUNTER(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,N,...) N
#
#  define VA_DISPATCHER(func, ...) VA_DISPATCHER_SOLVER(func, VA_ARG_SHIFTER(__VA_ARGS__))
#  define VA_DISPATCHER_SOLVER(func, nargs) VA_DISPATCHER_MERGER(func, nargs)
#  define VA_DISPATCHER_MERGER(func, nargs) func ## nargs
# endif
#
# define MakeTypeList01(T1) \
  Typelist<EXTRACTTYPE(T1), NullType>
# define MakeTypeList02(T1,T2) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList01(T2) >
# define MakeTypeList03(T1,T2,T3) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList02(T2,T3) >
# define MakeTypeList04(T1,T2,T3,T4) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList03(T2,T3,T4) >
# define MakeTypeList05(T1,T2,T3,T4,T5) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList04(T2,T3,T4,T5) >
# define MakeTypeList06(T1,T2,T3,T4,T5,T6) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList05(T2,T3,T4,T5,T6) >
# define MakeTypeList07(T1,T2,T3,T4,T5,T6,T7) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList06(T2,T3,T4,T5,T6,T7) >
# define MakeTypeList08(T1,T2,T3,T4,T5,T6,T7,T8) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList07(T2,T3,T4,T5,T6,T7,T8) >
# define MakeTypeList09(T1,T2,T3,T4,T5,T6,T7,T8,T9) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList08(T2,T3,T4,T5,T6,T7,T8,T9) >
# define MakeTypeList10(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList09(T2,,T3,T4,T5,T6,T7,T8,T9,T10) >
# define MakeTypeList11(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList10(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11) >
# define MakeTypeList12(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList11(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12) >
# define MakeTypeList13(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList12(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13) >
# define MakeTypeList14(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList13(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14) >
# define MakeTypeList15(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList14(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15) >
# define MakeTypeList16(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList15(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16) >
# define MakeTypeList17(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList16(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17) >
# define MakeTypeList18(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18) \
  Typelist<EXTRACTTYPE(T1), MakeTypeList17(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18) >
# define MakeTypeList19(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19) \
  TyplList<EXTRACTTYPE(T1), MakeTypeList18(T2,,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19) >

class NullType {};

template<class T, class U>
struct Typelist
{
    typedef T Head;
    typedef U Tail;
};

