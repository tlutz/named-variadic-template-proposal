# Old TypeList Version

## Principle

(ab)using macros to extract and separate types and names separately. The names are stored as an enum and behave as expected.

## Working features

* We can declare a list of type/name pairs
* The name is enclosed in the type
* The name is optional: a type can be anonymous
* get is behaving as expected

## Limitations

* Macros... macros everywhere...
* The type definition and the container definition are separated, could be fixed with more macros...
* We need parenthesis to define the type/name pairs, and there has to be a comma between the type and the name.
