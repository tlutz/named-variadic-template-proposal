#include "container.h"
#include <iostream>

int main() {
  // The type cannot be defined when instantiating the template
  typedef MakeTypeList((int, foo), (float, bar), (double, baz))Container_type;

  // The container type
  typedef Container<Container_type> AContainer;
  
  // A container instance
  AContainer container;

  // example of nametag access
  std::cout << "AContainer::foo = " << AContainer::foo << std::endl;

  // example of get usage
  container.get<AContainer::foo>({0,0,0});
}
