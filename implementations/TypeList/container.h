#include "Typelist.h"
#include <initializer_list>

template<typename TL>
struct Container : public TL {
  template<int index> void get(const std::initializer_list<std::size_t> &position){}
};
