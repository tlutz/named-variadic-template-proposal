# Implementations

Obviously none of these implementation solve the problem, otherwise it wouldn't be a C++ extension Proposal...

I tried several ways to come as close to the solution as possible and weighted the pros and cons of each approach.

The goals for the implementation are:

* Definition of a type aggregate and tried to associate a name to each type
* Try to access the name tags
* Try to access some elements of the type aggregate using its name