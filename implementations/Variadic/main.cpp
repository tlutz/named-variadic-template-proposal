#include "container.h"
#include <iostream>

// The name tags have to be pre-defined types
struct foo {};
struct bar {};
struct baz {};

int main() {
  // The name tags are interleaved with the types at instantiation
  typedef Container<int, foo, float, bar, double, baz> AContainer;

  // Could also be written as:
  // typedef Container<int,     struct foo, 
  //                   float,   struct bar, 
  //                   double,  struct baz> AContainer;

  // Accessing the tags cannot be done though the type
  // error: ‘foo’ is not a member of ‘AContainer
  // AContainer::foo;

  // Container instance
  AContainer container{1, 0.5f, 3.14d};
  
  // get behave as expected
  std::cout << "foo: " << container.get<foo>() << std::endl;
}
