# Variadic template and indexing typelist version

source: http://stackoverflow.com/a/13066078/401200

## Principle

A container is a heterogeneous type aggregate. It has a constructor which accept a variadic list of (even) arguments, split the arguments in odd/even, even ones are the true types, odd ones are the names.

## Working features

* The declaration allows for types and names.
* `get` is limited but the syntax is close to what we want.

## Limitations

* There has to be a comma between the type and its name.
* Names are mandatory for each type.
* The names have to be declared as types and are not enclosed in the type.
* The name name can be repeated.
