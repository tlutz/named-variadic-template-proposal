#pragma once

#include "named_tuple"

template<typename... Ts> struct Container:
  tt_impl<extract<2, 1, Ts...>, extract<2, 0, Ts...>> {
  template<typename... Args> Container(Args &&...args):
    tt_impl<extract<2, 1, Ts...>, extract<2, 0, Ts...>>(
      std::forward<Args>(args)...) {}
};
