#include <boost/fusion/include/map.hpp>
#include <boost/fusion/include/at_key.hpp>
using namespace boost::fusion;

int main(){
  // container type declaration
  typedef map<
    pair<struct foo, int>,
    pair<struct bar, float>,
    pair<struct baz, double>
  > AContainer;

  // error: ‘foo’ is not a member of ‘AContainer
  // std::cout << AContainer::foo << std::endl;

  // instantiation works
  AContainer m { 0, 3.14f, 0.1 };

  // get works
  at_key<foo>(m) = 0;
}

