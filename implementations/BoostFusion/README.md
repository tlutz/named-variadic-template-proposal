# Solution using Boost Fusion

## Principle

Use Boost pair to store a type as a key for an associative container

## Working features

* The declaration allows for types and names; they have to be wrapped in a pair.
* Getter is working
* Names have to be unique

## Limitations

* Notation still clumsy
* Names are mandatory
* The names are not enclosed in the type scope. Worse, they are enclosed in the declaring scope.
