This is a simple idea which could be turned into a C++ Standard proposal if enough people think it is a good idea. It is very immature for now.

Feel free to contribute to anything, the READMEs, the examples, the wiki, filing an issue, leaving a comment...

# Proposal for named variadic template parameters

# Motivations

Aims to decouple type, data layout and field name.

Data layout is a complex problem in High Performance Computing; perhaps the most well known problem is the struct-of-arrays versus arrays-of-struct. Writing high level abstractions for this kind of problem is much easier since variadic templates and tuples were introduced in c++11.

However the types in the aggregate have to be addressed by index, making code hardly readable and difficult to reason with, especially in the case of aggregation of typelists.

# Proposal

We propose to extend the template notation to accept a name tag immediately following the type. The name is an alias for the index of the template parameter. The names are inserted inside the scope of the created type.
The names are part of the type: two type aggregates with the same type list but different names are not of the same type.

# Example

Consider a container of type `<template ...Ts> Container`, which provides a proxy API for a multi-dimensional array-of-structs or a struct-of-arrays aggregate. This class provides a constructor and a `get` class which takes the dimensionality and coordinates respectively.

    // declare a container type with an int field and a float field
    typedef Container<int foo, float bar> AContainer;

    // The name tags are accessible as an enum and are aliases of their index
    assert(AContainer::foo == 0 && AContainer::bar == 1);
    
    // Allocate a container of the type AContainer with 1024^3 elements
    AContainer container{1024,1024,1024};

    // Get a reference on the int at origin
    container.get<foo>({0,0,0}) = 0;
    // get a reference on the float part at (1,1,1)
    container.get<bar>({1,1,1}) = 3.14f;
    // get the tuple element at (0,0,1)
    container.get({0,0,1}) = {0, 0.1f};

    // Merging containers is allowed, here using a + operator
    Container<double other> container2{1024,1024,1024};
    auto merged = container + container2;
    // merged is of type Container<int foo, float bar, double other>, and other is updated to the index
    assert(merged.other == 2);
    merged.get<merged.other>({0,1,2}) = 0.1;

    // Duplicate names should be compile time errors (same as enums)
    Container<int foo, int bar, int foo> error; // error: redeclaration of 'foo', previously declared at F:X:Y 
    auto merged = container + container; // error: redeclaration of 'foo', previously declared at F:X:Y

# Reference

* [1] is a similar proposal on the C++ Standard Proporals mailing list.
* [2] is a limited solution to achieve this.

[1]: https://groups.google.com/a/isocpp.org/forum/#!msg/std-proposals/N-kIXNrkTUk/s5iXjc7jzT8J
[2]: http://stackoverflow.com/a/13066078/401200

